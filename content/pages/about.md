+++
url = "/about"
title = "About Me"
date = "2023-11-05"
image = "/images/flower.jpg"
+++

Hey there! This is my creative space.

I enjoy making things like sculptures and other crafts, often as heartfelt gifts for others. Though I've shifted away from paintings in recent years, I still like to dabble with paint to enhance my other creations.

As for me, I'm not one to talk much about myself, but I'm happy to share my work with you. 
Take a look around, and I hope you enjoy what you find. Thanks for stopping by!
