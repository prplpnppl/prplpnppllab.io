+++
author = "Ange"
date = 2023-03-01
title = 'Pride Roses'
image = "/images/napkin-roses.jpg"
#description = ""
tags = ["pride", "flower"]
categories = ["napkin"]
draft = false
+++

Made with paper tissue, coloured with markers/acrylics.