+++
author = "Ange"
date = 2022-07-01
title = 'Basket'
image = "/images/pine-basket.jpg"
#description = ""
tags = ["gift", "decoration"]
categories = ["weaving", "acrylic"]
draft = false
+++

Made using long pine needles and a thread, coloured with acrylics.