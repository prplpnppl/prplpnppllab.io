+++
author = "Ange"
date = 2021-09-24
title = 'Hamster Coffin'
image = "/images/hamster-coffin.jpg"
#description = ""
tags = ["animal", "memorial"]
categories = ["cardboard"]
draft = false
+++

Cardboard coffin for a hamster.

Coloured with markers and fine liners