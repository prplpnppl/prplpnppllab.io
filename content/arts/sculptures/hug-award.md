+++
author = "Ange"
date = 2022-06-21
title = 'An award:)'
image = "/images/hug-award.jpg"
#description = ""
tags = ["gift", "monument", "hugs"]
categories = ["air dry clay", "pva glue", "acrylics", "fineliner"]
draft = false
+++

Made as a BD gift for my friend.